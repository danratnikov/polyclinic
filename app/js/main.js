/**
 * Слайдеры
 */
var PolyclinSliders = (function() {

  var $owlTop = $('#owl-top'), // верхний слайдер
      $owlMiddle = $('#owl-middle'); // слайдер посередине
    

  // настройки верхнего слайдера
  var optsTop = {
    items: 1,
    singleItem: true,
    pagination: true,
    navigations: true,
    rewindNav: true,
    responsive: true,
    autoPlay: true,
  };

  // настройки слайдера посередине
  var optMiddle = {
    items: 3,
    pagination: false,
    rewindNav: true,
    responsive: true,
    autoPlay: true,
  };

  function init() {
    var owlMiddleSlides = $('#owl-middle .item');

    if (optMiddle.items > owlMiddleSlides.length) {
      optMiddle.items = owlMiddleSlides.length;
      $('#owl-middle ~ .prev').addClass('hidden');
      $('#owl-middle ~ .next').addClass('hidden');
    }
    $owlTop.owlCarousel(optsTop);
    $owlMiddle.owlCarousel(optMiddle);
    _bindEvents();
  }

  function _bindEvents() {

    // обработчики кнопок управления
    $('#owl-top ~ .prev').on('click', function() {
      $owlTop.trigger('owl.prev');
    });

    $('#owl-top ~ .next').on('click', function() {
      $owlTop.trigger('owl.next');
    });
    $('#owl-middle ~ .prev').on('click', function() {
      $owlMiddle.trigger('owl.prev');
    });

    $('#owl-middle ~ .next').on('click', function() {
      $owlMiddle.trigger('owl.next');
    });

  }

  return {
    init: init
  };
})();


/**
 * Google карта
 */
var PolyclinMap = (function() {

  // основные настройки
  var lattitude = 55.865444, //широта
    longitude = 37.5853552, //долгота
    markerImage = '../img/map/map-marker.png'; //маркер

  var myLatLng = new google.maps.LatLng(lattitude, longitude);

  // доп. настройки
  var mapOpts = {
    center: myLatLng,
    panControl: true, 
    zoomControl: true,
    scrollwheel: false,
    scaleControl: false,
    streetViewControl: false,
    mapTypeControl: false,
    zoom: 17
  };

  function _initializeMap() {

    var map = new google.maps.Map(document.getElementById('map'), mapOpts);

    var beachMarker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: markerImage
    });
  }

  function init() {
    var map = google.maps.event.addDomListener(window, 'load', _initializeMap);
  }

  return {
    init: init
  };
})();


// Точка входа
$(document).ready(function() {
  PolyclinSliders.init(); // запускаем слайдеры
  PolyclinMap.init(); // запускаем карту
});