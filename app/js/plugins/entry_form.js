

/* Функции проверки полей */
$(function(){

	$("#entry__form #phone").mask("+7 (999) 999-99-99");
	$("#entry__form").submit(function() {
		var error=0;

		var phone = $("#entry__form #phone").val();
		if(phone==''){
			error=2;
			$("#entry__form #phone").parent('div').addClass("has-error");
		}
		else
		{
			$("#entry__form #phone").parent('div').removeClass("has-error");
		}

		var author = $("#entry__form #author").val();
		//alert(autor);
		if(author==''||author=='ИМЯ'){
			error=2;
			$("#entry__form #author").parent('div').addClass("has-error");
		}
		else
		{
			$("#entry__form #author").parent('div').removeClass("has-error");
		}

		if (error==0){
			$("#entry__form").ajaxSubmit({
				url: '/entry/',
				type: 'post',
				success:    function(data) {
					$('#entry__form').html(data);
					$('#entry__form form').hide();
				}
			});
		}
		return false;
	});

});

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}



/* Вызов функции select */
(function($) {$(function() {$('.standart-form select').selectbox();})})(jQuery)