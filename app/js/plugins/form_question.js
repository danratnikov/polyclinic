

/* Функции проверки полей */
$(function(){
	$("#question__form #phone").mask("+7 (999) 999-99-99");
	$("#question__form").submit(function() {
	var error=0;
		var phone = $("#question__form #phone").val();
		if(phone==''){
			error=2;
			$("#question__form #phone").parent('div').addClass("has-error");
		}
		else
		{
			$("#question__form #phone").parent('div').removeClass("has-error");
		}

	var author = $("#question__form #author").val();

		if(author==''||author=='ВАШЕ ИМЯ'){
			error=2;
			$("#question__form #author").parent('div').addClass("has-error");
		}
		else
		{
			$("#question__form #author").parent('div').removeClass("has-error");
		}
		
	var email = $("#question__form #email").val();
		if(!isValidEmailAddress(email)){
			error=2;
			$("#question__form #email").parent('div').addClass("has-error");
		}
		else
		{
			$("#question__form #email").parent('div').removeClass("has-error");
		}	
		
		if (error==0){
		$.loadingScreen('show');	
		$("#question__form").ajaxSubmit({
            url: '/ask-question/',
            type: 'post',
            success:    function(data) {
            $.loadingScreen('hide');
            $('#question-window').html(data);
			$('#question__form').hide();
            }
			});
		}
		return false;	
	});
	
	
	
	
		/* Закрыть popup окно */
	//$(".popup-window .close, .popup-window .abort").click(function () {
	//	$(".popup-window").fadeOut("medium");
	//	$("#button-question-window").removeClass("current");
	//	$("#make-appoint").removeClass("current");
	//	$(".add-review-link").removeClass("current");
	//});
	
});

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
}

/* Вызов функции select */
(function($) {$(function() {$('form select').selectbox();})})(jQuery)